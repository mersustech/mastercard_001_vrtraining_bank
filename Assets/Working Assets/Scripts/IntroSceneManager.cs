﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class IntroSceneManager : MonoBehaviour
{
    [SerializeField] private VideoPlayer r_VideoPlayer;
    [SerializeField] private GameObject r_VideoScreen;
    

    // Start is called before the first frame update
    void Start()
    {

        ReadyUpVideoPlayer();
        StartCoroutine("WaitAndPlay",4f);
    }

    IEnumerator WaitAndPlay(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        r_VideoPlayer.Prepare();

        while (!r_VideoPlayer.isPrepared)
        {
            yield return null;
        }

        r_VideoPlayer.frame = 0;
        r_VideoPlayer.Play();
        r_VideoScreen.SetActive(true);

        while (r_VideoPlayer.isPlaying)
        {
            yield return null;
        }

        yield return new WaitForSeconds(.5f);

        SceneManager.LoadSceneAsync(1, LoadSceneMode.Single);
    }

    void ReadyUpVideoPlayer()
    {
        r_VideoScreen.SetActive(true);
    }
}
